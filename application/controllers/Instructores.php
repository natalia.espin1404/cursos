<?php

class Instructores extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('instructor');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('instructores/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['instructores']=$this->instructor->obtenerTodos();
    $this->load->view('header');
    $this->load->view('instructores/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoInstructor=array(
      "cedula_ins"=>$this->input->post('cedula_ins'),
      "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
      "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
      "nombres_ins"=>$this->input->post('nombres_ins'),
      "titulo_ins"=>$this->input->post('titulo_ins'),
      "telefono_ins"=>$this->input->post('telefono_ins'),
      "direccion_ins"=>$this->input->post('direccion_ins')

    );
    if($this->instructor->insertar($datosNuevoInstructor)){
      redirect('instructores/index');

    }else {
      echo "<h1>ERRORE AL INSERTAR </h1>";
    }

  }
  //funcion para Eliminar Instructores
  //metodo get
  public function eliminar($id_ins){
   if ($this->instructor->borrar($id_ins)) { //invocando al modelo
     redirect('instructores/index');
   } else {
     echo "ERROR AL BORRAR :(";
   }

  }

}//NO borrar el cierre de la clase


 ?>
