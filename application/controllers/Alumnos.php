<?php

class Alumnos extends CI_Controller
{
  //Constructor
  function __construct()
  {
    parent::__construct();
    //cargar modelo

    $this->load->model('alumno');
  }
  //Renderizacion de la vista que
  //muestra los desayunos
  public function nuevo(){
    $this->load->view('header');
    $this->load->view('alumnos/nuevo');
    $this->load->view('footer');

  }
  public function index(){
    $data['alumnos']=$this->alumno->obtenerTodos();
    $this->load->view('header');
    $this->load->view('alumnos/index',$data);
    $this->load->view('footer');

  }
  public function guardar(){
    $datosNuevoAlumno=array(
      "nombre_alum"=>$this->input->post('nombre_alum'),
      "apellido_alum"=>$this->input->post('apellido_alum'),
      "nombre_curso_alum"=>$this->input->post('nombre_curso_alum'),
      "edad_alum"=>$this->input->post('edad_alum')
    );
    if($this->alumno->insertar($datosNuevoAlumno)){
      redirect('alumnos/index');

    }else {
      echo "<h1>ERRORE AL INSERTAR </h1>";
    }

  }
  //funcion para Eliminar Alumnos
  //metodo get
  public function eliminar($id_alum){
   if ($this->alumno->borrar($id_alum)) { //invocando al modelo
     redirect('alumnos/index');
   } else {
     echo "ERROR AL BORRAR :C";
   }

  }

}//NO borrar el cierre de la clase


 ?>
