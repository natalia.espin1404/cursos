<?php
  class Alumno extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("alumnos",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoAlumnos=
      $this->db->get("alumnos");
      if($listadoAlumnos->num_rows()>0){ //si hay datos
      return $listadoAlumnos->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar Alumno
    function borrar($id_alum){
      $this->db->where("id_alum",$id_alum);
      if ($this->db->delete("alumnos")) {
        return true;
      } else {
        return false;
      }

    }
  }//Cierre de la clase

 ?>
