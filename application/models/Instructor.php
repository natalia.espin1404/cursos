<?php
  class Instructor extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db->insert("instructor",$datos);
    }
    //Funcion para consultar Instructores
    function obtenerTodos(){
      $listadoInstructores=
      $this->db->get("instructor");
      if($listadoInstructores->num_rows()>0){ //si hay datos
      return $listadoInstructores->result();
      }else { //no hay datos
      return false;
      }
    }
    //borrar instructor
    function borrar($id_ins){
      $this->db->where("id_ins",$id_ins);
      if ($this->db->delete("instructor")) {
        return true;
      } else {
        return false;
      }

    }
  }//Cierre de la clase

 ?>
