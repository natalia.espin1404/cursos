<h1>Listado de Alumnos</h1>
<br>
<?php if ($alumnos): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th> APELLIDO</th>
        <th>NOMBRE DEL CURSO </th>
        <th>EDAD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($alumnos as $filaTemporal): ?>
        <tr>
          <td>
            <?php echo $filaTemporal->id_alum ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_alum ?>
          </td>
          <td>
            <?php echo $filaTemporal->apellido_alum ?>
          </td>
          <td>
            <?php echo $filaTemporal->nombre_curso_alum ?>
          </td>
          <td>
            <?php echo $filaTemporal->edad_alum ?>
          </td>
          <td class="text-center">
            <a href="#" title="Editar Alumno"
            style="color:blue;">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            &nbsp;&nbsp;&nbsp;
            <a href="<?php echo site_url(); ?>/alumnos/eliminar/<?php echo $filaTemporal->id_alum ?>" title="Eliminar Alumno"
            style="color:red;">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>

    </tbody>

  </table>
<?php else: ?>
  <h1>No hay Alumnos</h1>
<?php endif; ?>
