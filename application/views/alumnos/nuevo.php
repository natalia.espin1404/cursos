<h1>NUEVO ALUMNO</h1>
<form class=""
action="<?php echo site_url(); ?>/alumnos/guardar"
method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre"
          class="form-control"
          name="nombre_alum" value=""
          id="nombre_alum">
      </div>
      <div class="col-md-4">
          <label for=""> Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el  apellido"
          class="form-control"
          name="apellido_alum" value=""
          id="apellido_alum">
      </div>
      <div class="col-md-4">
        <label for="">Nombre del Curso:</label>
        <br>
        <input type="text"
        placeholder="Ingrese el nombre del curso"
        class="form-control"
        name="nombre_curso_alum" value=""
        id="nombre_curso_alum">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
          <label for="">Edad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la edad"
          class="form-control"
          name="edad_alum" value=""
          id="edad_alum">
      </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/alumnos/index"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
</form>
